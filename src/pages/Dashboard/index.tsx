import './styles'
import { Title, Form, Repos } from './styles';
import logo from '../../assets/logo.svg';
import { FiChevronRight } from 'react-icons/fi';
import { api } from '../../services/api';
import React, { ChangeEvent } from 'react';

interface GitHubApi{
    full_name:string;
    description:string;
    owner: {
        login:string;
        avatar_url:string;
    }
}


export const Dashboard: React.FC = () => {
    const [repos, setRepos] = React.useState<GitHubApi[]>(() => {
        const storageRepos = localStorage.getItem('@gitCol'); //Buscando do local storage
        if (storageRepos) {                                 //Caso não exista, retorna array vazio
            return JSON.parse(storageRepos)
        }
        return [];
    });
    const [newRepo, setNewRepo] = React.useState('');
    const [inputValidation, setInputValidation] = React.useState('');
    const [inputValidationColor, setinputValidationColor] = React.useState('');
    const styles = {
        color: inputValidationColor
    };

    //Chamando evento ao alterar o valor de uma variável
    React.useEffect(() => {
        localStorage.setItem('@gitCol',JSON.stringify(repos));
    }, [repos]);

    function handleInputChange(event: React.ChangeEvent<HTMLInputElement>):void
    {
        setNewRepo(event.target.value);
    }

    async function handleAddRepo(event: React.FormEvent<HTMLFormElement>): Promise<void>
    {
        event.preventDefault();

        if (!newRepo) {
            setInputValidation('Informe o username/repositório');
            setinputValidationColor('red');
            return;
        }
        const response = await api.get<GitHubApi>(`repos/${newRepo}`).then((response) => {
            setInputValidation('Projeto encontrado com sucesso.');
            setinputValidationColor('green');
            const repository = response.data;
            setRepos([...repos, repository]);
            setNewRepo('');
        }).catch((error) => {
            setInputValidation('Não foi possível encontrar o projeto no github.');
            setinputValidationColor('red');
        });

    
    }

    return (
        <>
        <img src={logo} alt="gitCollection logo" />
        <Title>Catálogo de repositórios</Title>
        <Form onSubmit={handleAddRepo}>
            <input placeholder="username/repository" onChange={handleInputChange} />
            <button type="submit" >Buscar</button>
        </Form>
        <p style={styles}>{inputValidation}</p>
        <Repos>
            {repos.map(repository => (
                <a href={`/repositories/${repository.full_name}`} key={repository.full_name}>
                <img 
                    src={repository.owner.avatar_url} 
                    alt={repository.owner.login} />
                <div> 
                    <strong>{repository.full_name}</strong>
                    <p>{repository.description}</p>
                </div>
                <FiChevronRight size={20} ></FiChevronRight>
            </a>
            ))
            }
        </Repos>
        
        </>
    );
  };
  
  