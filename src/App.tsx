import {BrowserRouter} from 'react-router-dom';
import {Routes} from './routes';
import {GlobalStyle} from './styles/global';

function App() {
  return (
    <>
    <BrowserRouter>
      <Routes></Routes>
    </BrowserRouter>
    <GlobalStyle></GlobalStyle>
    </>
  );
}

export default App;
